import React from 'react';
import {RootNavigator} from './rootNavigator';
import {NavigationContainer} from '@react-navigation/native';
import {Provider} from 'react-redux';
import {PersistGate} from 'redux-persist/integration/react';
import {store, persistor} from '~store';
import {CompanyProvider} from '~context/company';
import Toast from 'react-native-toast-message';

import '~utils/types'; // load custom type_name colors

const App = () => {
  return (
    <Provider store={store}>
      <CompanyProvider>
        <NavigationContainer>
          <PersistGate persistor={persistor} loading={null}>
            <RootNavigator />
          </PersistGate>
          <Toast ref={ref => Toast.setRef(ref)} />
        </NavigationContainer>
      </CompanyProvider>
    </Provider>
  );
};

export default App;
