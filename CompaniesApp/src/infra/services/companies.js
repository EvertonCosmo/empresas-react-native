import env from '~config/env';
import {create} from 'apisauce';
import {getStorage} from '~utils/storage';

const Api = create({
  baseURL: `${env.COMPANY_BASE_URL}`,
});
Api.addResponseTransform(response => {
  if (!response.ok) {
    throw response;
  }
});
// Api.addAsyncRequestTransform(request => async () => {
//   const {access_token, client, uid} = await getStorage([
//     '@acessToken',
//     '@client',
//     '@uid',
//   ]);
//   if (access_token && client && uid) {
//     Api.setHeader('access-token', access_token);
//     Api.setHeader('client', client);
//     Api.setHeader('uid', uid);
//   }
// });
const signIn = async ({email, password}) => {
  const response = await Api.post(
    `${env.COMPANY_BASE_URL}/api/v1/users/auth/sign_in`,
    {
      email,
      password,
    },
  );

  return response;
};
const getAllCompanies = async () => {
  const {access_token, client, uid} = await getStorage([
    '@acessToken',
    '@client',
    '@uid',
  ]);
  let headers = {
    'access-token': access_token,
    client: client,
    uid: uid,
  };
  const response = await Api.get('/api/v1/enterprises', {}, {headers: headers});
  return response;
};
const getById = async ({id}) => {
  const {access_token, client, uid} = await getStorage([
    '@acessToken',
    '@client',
    '@uid',
  ]);
  let headers = {
    'access-token': access_token,
    client: client,
    uid: uid,
  };
  const response = await Api.get(
    `/api/v1/enterprises/${id}`,
    {},
    {headers: headers},
  );
  return response;
};
const search = async ({type, name}) => {
  const {access_token, client, uid} = await getStorage([
    '@acessToken',
    '@client',
    '@uid',
  ]);
  let headers = {
    'access-token': access_token,
    client: client,
    uid: uid,
  };
  let params = {
    name: name,
    enterprise_types: type,
  };
  if (name === '') {
    delete params.name;
  }
  if (type === 1) {
    delete params.enterprise_types;
  }

  const response = await Api.get('/api/v1/enterprises', params, {
    headers: headers,
  });
  return response;
};
const api = {
  signIn,
  getAllCompanies,
  getById,
  search,
};
export {api};
