import React, {Fragment} from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {useSelector} from 'react-redux';
import {AuthStack} from '~pages/Auth';
import {MainStack} from '~pages/Main';
import Spinner from 'react-native-loading-spinner-overlay';
const Stack = createNativeStackNavigator();

const RootNavigator = () => {
  const {isAuthenticated, loading} = useSelector(state => state.session);
  return (
    <Fragment>
      <Stack.Navigator screenOptions={{headerShown: false}}>
        {isAuthenticated ? (
          <Stack.Screen name={'main-app'} component={MainStack} />
        ) : (
          <Stack.Screen name={'auth-app'} component={AuthStack} />
        )}
      </Stack.Navigator>
      <Spinner
        visible={loading}
        textContent={'Loading...'}
        // eslint-disable-next-line react-native/no-inline-styles
        textStyle={{color: '#fff'}}
      />
    </Fragment>
  );
};

export {RootNavigator};
