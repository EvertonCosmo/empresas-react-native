import React from 'react';
import {StyleSheet, View} from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

import {useCompany} from '~context/company';

import * as Styles from './styles';
const Search = () => {
  const {setSearchTerm} = useCompany();

  return (
    <Styles.Wrapper>
      <Styles.Input
        style={styles.inputShadow}
        maxLength={14}
        placeholder={'Search'}
        onChangeText={setSearchTerm}
        placeholderTextColor={'#868686'}
      />
      <Styles.WrapperIcon>
        <MaterialIcons color={'#fff'} name={'search'} size={18} />
      </Styles.WrapperIcon>
    </Styles.Wrapper>
  );
};
const styles = StyleSheet.create({
  inputShadow: {
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0,
    shadowRadius: 4.65,

    elevation: 8,
  },
});
export default Search;
