import styled from 'styled-components/native';

export const Input = styled.TextInput`
  flex: 1;
  justify-content: center;
  align-items: center;
  border-radius: 18px;
  width: 100%;
  font-size: 18px;
  background-color: #fff;
  padding-left: 16px;

  /* height: 50px; */
  /* margin: 20px; */
  color: #52665a;
  border-color: #cfcfcf;
`;

export const Wrapper = styled.View`
  /* background-color: green; */
  flex-direction: row;
  align-items: center;
  justify-content: flex-end;
  width: 100%;
  border-radius: 18px;
`;
export const WrapperIcon = styled.View`
  position: absolute;
  right: 5px;
  align-items: center;
  justify-content: center;
  border-radius: 80px;
  width: 40px;
  height: 40px;
  background-color: #3482ff;
  elevation: 9;
`;
