import React from 'react';
import {Text, StyleSheet, TouchableOpacity} from 'react-native';
import {RectButton} from 'react-native-gesture-handler';
const Button = ({text, ...rest}) => {
  return (
    <TouchableOpacity style={styles.button} activeOpacity={0.7} {...rest}>
      <Text style={styles.text}> {text}</Text>
    </TouchableOpacity>
  );
};
const styles = StyleSheet.create({
  button: {
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#3482FF',
    height: 50,
    width: '100%',
  },
  text: {
    fontSize: 24,
    color: 'white',
  },
});
export default Button;
