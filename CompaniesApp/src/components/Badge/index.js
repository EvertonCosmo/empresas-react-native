import React from 'react';
import {enterpriseTypes} from '~utils/types';
import * as Styles from './styles';

const Badge = ({text, ...rest}) => {
  return (
    <Styles.Badge {...rest} background={enterpriseTypes[text]}>
      <Styles.Text>{text} </Styles.Text>
    </Styles.Badge>
  );
};
export default Badge;
