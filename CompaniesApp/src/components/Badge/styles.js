import styled from 'styled-components/native';

export const Badge = styled.TouchableOpacity`
  display: flex;
  flex-direction: row;
  align-items: center;
  width: 120px;
  justify-content: space-around;
  padding: 8px;
  margin-right: 10px;
  background: ${props => props.background || 'gray'};
  border-radius: 10px;
`;
export const Text = styled.Text`
  color: #fff;
  font-weight: bold;
  font-size: 16px;
`;
