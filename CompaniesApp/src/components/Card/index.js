import React from 'react';
import {Image, StyleSheet, Text, View, TouchableOpacity} from 'react-native';

import {useNavigation} from '@react-navigation/core';

import Badge from '~components/Badge';
import {Creators as companyCreators} from '~store/ducks/company';
import {useDispatch} from 'react-redux';
const Card = ({id, name, city, country, sharePrice, type, photo}) => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  function navigate() {
    navigation.navigate('info');
    dispatch(companyCreators.getById({id}));
  }
  return (
    <TouchableOpacity
      activeOpacity={0.5}
      onPress={navigate}
      style={styles.container}>
      <Image
        style={styles.image}
        source={{
          uri: photo,
        }}
        resizeMode={'cover'}
      />

      <View style={styles.informations}>
        <Text style={styles.title}>{name}</Text>
        <View>
          <Text>{city}</Text>
          {/* {getFlag(country)} */}
          <Text>{country}</Text>
        </View>
        <View style={styles.footer}>
          <Badge text={type} />
        </View>
      </View>
    </TouchableOpacity>
  );
};
const styles = StyleSheet.create({
  container: {
    backgroundColor: '#FFF',
    maxWidth: '100%',
    flex: 1,
    borderRadius: 20,
    paddingVertical: 10,
    margin: 10,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 10,
    elevation: 8,
  },
  image: {
    borderRadius: 20,
    width: 100,
    height: 100,
  },
  informations: {
    flex: 1,
    paddingHorizontal: 20,
  },
  title: {
    fontWeight: 'bold',
    fontSize: 18,
  },
  footer: {
    flexDirection: 'row',
    marginTop: 8,
  },
});
export default Card;
