import React from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  // Image,
  TouchableOpacity,
} from 'react-native';
import {StatusBar} from 'react-native';
import {View} from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {useDispatch, useSelector} from 'react-redux';
// import defaultImage from '~assets/tiger.jpeg';
import {getGrettings} from '~utils/grettings';
import {Creators as sessionCreators} from '~store/ducks/session';
export function Header() {
  const dispatch = useDispatch();

  const {investor} = useSelector(state => state.session);

  return (
    <View style={styles.container}>
      <View>
        <Text style={styles.greeting}>{getGrettings()}</Text>
        <Text style={styles.name}>{investor?.investor_name}</Text>
      </View>
      {/* <Image source={defaultImage} style={styles.image} /> */}
      <TouchableOpacity
        onPress={() => dispatch(sessionCreators.logout())}
        activeOpacity={0.7}>
        <MaterialIcons name={'logout'} color={'black'} size={40} />
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: Platform.OS === 'android' ? StatusBar.currentHeight : 0,
  },
  image: {
    width: 70,
    height: 70,
    borderRadius: 40,
  },
  greeting: {
    color: '#52665A',
    fontSize: 16,
  },
  name: {
    color: '#000',
    fontSize: 32,
    fontWeight: 'bold',
    lineHeight: 40,
  },
});
export default Header;
