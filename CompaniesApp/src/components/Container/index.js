import React from 'react';

import * as Styles from './styles';
export function Container({children}) {
  return <Styles.Container>{children}</Styles.Container>;
}
export default Container;
