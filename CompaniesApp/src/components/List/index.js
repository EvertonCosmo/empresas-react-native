import React from 'react';
import {FlatList} from 'react-native';
import {useDispatch} from 'react-redux';
import {Creators as companyCreators} from '~store/ducks/company';
import Badge from '~components/Badge';
import Card from '~components/Card';
import env from '~config/env';
import {useCompany} from '~context/company';
function List({data, badges, ...rest}) {
  const {setSelectedBadge} = useCompany();
  const dispatch = useDispatch();

  function renderItemCard(item) {
    return (
      <Card
        id={item?.id}
        name={item?.enterprise_name}
        city={item?.city}
        country={item?.country}
        photo={`${env.COMPANY_BASE_URL}${item.photo}`}
        sharePrice={item?.share_price}
        type={item?.enterprise_type?.enterprise_type_name}
      />
    );
  }
  function renderItemBadge(item) {
    function filterByBadge(id) {
      if (id === 1) {
        dispatch(companyCreators.getAll(false));
      } else {
        dispatch(companyCreators.search({type: id}));
      }
    }

    return (
      <Badge
        key={String(item?.id)}
        text={item?.name}
        id={item?.id}
        onPress={() => {
          filterByBadge(item?.id);
          setSelectedBadge(item?.id);
        }}
      />
    );
  }
  return (
    <FlatList
      keyExtractor={item => item.id}
      data={data}
      initialNumToRender={5}
      contentContainerStyle={{paddingVertical: 16}}
      renderItem={({item}) =>
        badges ? renderItemBadge(item) : renderItemCard(item)
      }
      {...rest}
    />
  );
}
export default List;
