import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import SignIn from './SignIn';

const Stack = createNativeStackNavigator();

const AuthStack = () => {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Group screenOptions={{presentation: 'modal'}}>
        <Stack.Screen name={'sign-in'} component={SignIn} />
      </Stack.Group>
    </Stack.Navigator>
  );
};
export {AuthStack};
