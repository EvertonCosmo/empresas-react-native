import React, {useEffect, useState} from 'react';
import {StyleSheet, View, Platform} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {Creators as sessionCreators} from '~store/ducks/session';
import Toast from 'react-native-toast-message';
import {Button} from '~components';

import * as Styles from './styles';

const SignIn = () => {
  const dispatch = useDispatch();

  const [email, setEmail] = useState(null);
  const [password, setPassord] = useState(null);

  function login() {
    dispatch(
      sessionCreators.signIn({
        email: email,
        password: password,
      }),
    );
  }
  return (
    <Styles.KeyboardAvoid
      behavior={Platform.OS === 'ios' ? 'padding' : 'height'}>
      <Styles.Container>
        <Styles.Body>
          <Styles.LogoContainer>
            <Styles.Image
              resizeMode={'contain'}
              source={require('~assets/background.png')}
            />
          </Styles.LogoContainer>
          <View style={styles.startTextContainer}>
            <Styles.LoginNow> Login Now </Styles.LoginNow>
            {/* <Text style={styles.subLoginText}>
              Please enter the details bellow to continue
            </Text> */}
          </View>
          <View>
            <Styles.Input
              placeholderTextColor={'#868686'}
              placeholder={'Email'}
              onChangeText={setEmail}
            />
            <Styles.Input
              placeholderTextColor={'#868686'}
              placeholder={'Password'}
              onChangeText={setPassord}
              secureTextEntry
            />
          </View>

          <Button text={'Login'} onPress={() => login()} />
        </Styles.Body>
      </Styles.Container>
    </Styles.KeyboardAvoid>
  );
};
const styles = StyleSheet.create({
  startTextContainer: {
    width: '100%',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default SignIn;
