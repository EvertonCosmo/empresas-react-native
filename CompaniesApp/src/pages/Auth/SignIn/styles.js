import styled from 'styled-components/native';

export const Input = styled.TextInput`
  border-radius: 20px;
  background-color: #f0f0f0;
  color: #868686;
  width: 300px;
  padding-left: 18px;
  margin-bottom: 18px;
  font-size: 18px;
`;
export const KeyboardAvoid = styled.KeyboardAvoidingView`
  flex: 1;
  /* background-color: green; */
  align-items: center;
  justify-content: center;
  background-color: #fff;
`;
export const Container = styled.View`
  flex: 1;
  /* background-color: blue; */
  justify-content: center;
  align-items: center;
`;
export const LogoContainer = styled.View`
  flex: 0.4;
  /* top: 8%; */

  /* margin-top: 15px; */
  margin-bottom: 40px;
  justify-content: center;
  align-items: center;
`;
export const Body = styled.View`
  flex: 1;
  width: 300px;
  align-items: center;
  justify-content: center;
`;
export const LoginNow = styled.Text`
  font-weight: bold;
  font-size: 32px;
  margin-bottom: 10px;
`;
export const Image = styled.Image`
  width: 300px;
  height: 140px;
`;
