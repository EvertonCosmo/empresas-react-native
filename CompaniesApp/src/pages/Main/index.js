import {createNativeStackNavigator} from '@react-navigation/native-stack';
import React from 'react';
import {Home} from './Home';
import {CompanyInfo} from './Home/CompanyInfo';

const Stack = createNativeStackNavigator();

const MainStack = () => {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name={'home'} component={Home} />
      <Stack.Screen name={'info'} component={CompanyInfo} />
    </Stack.Navigator>
  );
};
export {MainStack};
