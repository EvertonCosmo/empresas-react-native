import React, {useEffect} from 'react';
import {
  Text,
  View,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {ImageBackground} from 'react-native';
import {useSelector} from 'react-redux';
import {Badge, Container} from '~components';
import env from '~config/env';
import {useNavigation} from '@react-navigation/core';

const CompanyInfo = () => {
  const navigation = useNavigation();
  const {enterprise} = useSelector(state => state.company);
  function backNavigation() {
    navigation.goBack();
  }
  return (
    <Container>
      <ImageBackground
        resizeMode={'cover'}
        style={styles.image}
        source={{
          uri: `${env.COMPANY_BASE_URL}${enterprise?.photo}`,
        }}>
        <View style={styles.headerContainer}>
          <TouchableOpacity
            activeOpacity={0.7}
            style={styles.iconCircle}
            onPress={backNavigation}>
            <MaterialIcons name={'arrow-back'} size={24} color={'#fff'} />
          </TouchableOpacity>
        </View>
        <View style={styles.body}>
          <View style={styles.companyTitle}>
            <Text style={styles.companyName}>
              {enterprise?.enterprise_name}{' '}
            </Text>
          </View>
          <ScrollView
            contentContainerStyle={{paddingBottom: 200}}
            style={styles.scrollView}
            showsVerticalScrollIndicator={false}>
            <View style={styles.infoLine}>
              <View style={styles.infoCard}>
                <MaterialIcons
                  name={'location-city'}
                  size={32}
                  color={'black'}
                />
                <Text>{enterprise?.city}</Text>
              </View>

              <View style={styles.infoCard}>
                <MaterialIcons name={'flag'} size={32} color={'black'} />
                <Text> {enterprise?.country}</Text>
              </View>

              <View style={styles.infoCard}>
                <MaterialIcons name={'share'} size={32} color={'black'} />
                <Text>{enterprise?.share_price} </Text>
              </View>
            </View>
            <View style={styles.description}>
              <View style={styles.badgeLine}>
                <Text style={styles.title}> Description</Text>
                <Badge
                  text={enterprise?.enterprise_type?.enterprise_type_name}
                />
              </View>

              <Text style={styles.descriptionText}>
                {enterprise?.description}
              </Text>
            </View>
          </ScrollView>
        </View>
      </ImageBackground>
    </Container>
  );
};
const styles = StyleSheet.create({
  headerContainer: {
    marginTop: 30,
    paddingHorizontal: 18,
  },
  badgeLine: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingBottom: 20,
    // paddingVertical: 18,
    // paddingHorizontal: 18,
  },
  companyTitle: {
    marginTop: 24,
    paddingHorizontal: 24,
    alignItems: 'center',
    justifyContent: 'center',
  },
  companyName: {
    fontSize: 32,
    fontWeight: 'bold',
    color: 'black',
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
  },
  scrollView: {
    flex: 1,
    // transform: [{translateY: 70}],
    borderTopLeftRadius: 60,

    borderTopRightRadius: 60,
  },
  body: {
    overflow: 'hidden',
    flex: 1,
    // height: 1000,
    backgroundColor: 'white',
    transform: [{translateY: 90}],
    borderTopLeftRadius: 60,
    borderTopRightRadius: 60,
  },
  description: {
    paddingHorizontal: 20,
    flex: 1,

    // height: 880,
  },
  descriptionText: {
    fontWeight: '400',
    fontSize: 16,
  },
  image: {
    flex: 1,
    resizeMode: 'cover',
    height: 300,
    justifyContent: 'center',
  },
  iconCircle: {
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 40,
    height: 48,
    width: 48,
    backgroundColor: '#4c4e52',
    color: 'white',
  },
  infoLine: {
    marginTop: 30,
    flexDirection: 'row',
    justifyContent: 'space-between',
    margin: 10,
    paddingHorizontal: 20,
  },
  infoCard: {
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
    height: 90,
    width: 90,
  },
});
export {CompanyInfo};
