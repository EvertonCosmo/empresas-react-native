import React, {useEffect} from 'react';

import {StyleSheet, View} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {Container, Search} from '~components';
import Header from '~components/Header';
import List from '~components/List';
import {Creators as companyCreators} from '~store/ducks/company';

const Home = () => {
  const dispatch = useDispatch();

  const {enterprises, types} = useSelector(state => state.company);

  useEffect(() => {
    dispatch(companyCreators.getAll());
  }, []);
  return (
    <Container>
      <View style={styles.header}>
        <Header />
      </View>

      <View style={styles.searchBar}>
        <Search />
      </View>
      <View style={styles.badges}>
        <List
          badges
          horizontal
          data={types}
          showsHorizontalScrollIndicator={false}
        />
      </View>
      <View style={styles.cards}>
        <List badges={false} data={enterprises} />
      </View>
    </Container>
  );
};
const styles = StyleSheet.create({
  header: {
    paddingHorizontal: 18,
  },
  searchBar: {
    paddingHorizontal: 18,
    paddingVertical: 18,
    marginTop: 30,
  },
  badges: {
    paddingHorizontal: 18,
  },
  cards: {
    flex: 1,
  },
});
export {Home};
