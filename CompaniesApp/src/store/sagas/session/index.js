import {all, call, delay, put, takeLatest} from 'redux-saga/effects';
import {Types} from '~store/ducks/session';
import {api} from '~infra/services/companies';
import {setStorage} from '~utils/storage';
import Toast from 'react-native-toast-message';

export function* signIn({payload}) {
  try {
    yield put({type: Types.SESSION_LOADING});
    const {data, headers} = yield call(api.signIn, payload);

    setStorage(headers['access-token'], '@acessToken');
    setStorage(headers?.client, '@client');
    setStorage(headers?.uid, '@uid');
    yield put({
      type: Types.SESSION_SIGNIN_SUCCESS,
      payload: {
        investor: data?.investor,
        enterprise: data?.enterprise,
      },
    });
  } catch (error) {
    Toast.show({
      type: 'error',
      text1: 'Error',
      text2: error?.data?.errors[0],
    });
    yield put({
      type: Types.SESSION_SIGNIN_ERROR,
      payload: {error: error?.data?.errors[0]},
    });
  }
}
export function* logout() {
  try {
    yield put({type: Types.SESSION_LOADING});
    yield delay(300);
    yield put({type: Types.SESSION_LOGOUT_SUCCESS});
  } catch (error) {
    yield put({type: Types.SESSION_LOGOUT_ERROR, error: error.message});
  }
}
export default all([
  takeLatest(Types.SESSION_SIGNIN_REQUEST, signIn),
  takeLatest(Types.SESSION_LOGOUT_REQUEST, logout),
]);
