import {all, call, put, takeLatest} from 'redux-saga/effects';
import Toast from 'react-native-toast-message';
import {api} from '~infra/services/companies';
import {Types} from '~store/ducks/company';
export function* getAllTypes({payload}) {
  const {data} = payload;
  let types = data?.enterprises
    .reduce((acc, cur) => {
      const removed = acc.filter(el => el?.id !== cur.enterprise_type?.id);
      return [
        ...removed,
        {
          name: cur.enterprise_type?.enterprise_type_name,
          id: cur.enterprise_type?.id,
        },
      ];
    }, [])
    .sort((a, b) => a.id - b.id);

  types.unshift({name: 'All', id: 1});

  yield put({type: Types.COMPANY_SAVE_TYPES_SUCCESS, payload: {types}});
}
export function* getAll({payload}) {
  const {isGetTypes} = payload;

  try {
    const {data} = yield call(api.getAllCompanies);

    yield all(
      isGetTypes
        ? [
            put({type: Types.COMPANY_SAVE_TYPES, payload: {data}}),
            put({
              type: Types.COMPANY_GETALL_SUCCESS,
              payload: {enterprises: data?.enterprises},
            }),
          ]
        : [
            put({
              type: Types.COMPANY_GETALL_SUCCESS,
              payload: {enterprises: data?.enterprises},
            }),
          ],
    );
  } catch (error) {
    Toast.show({
      type: 'error',
      text1: 'Error',
      text2: error?.data?.errors[0],
    });
    yield put({
      type: Types.SESSION_SIGNIN_ERROR,
      payload: {error: error?.data?.errors[0]},
    });
  }
}

export function* getById({payload}) {
  try {
    const {data} = yield call(api.getById, payload);

    yield put({
      type: Types.COMPANY_FILTER_BYID_SUCCESS,
      payload: {
        enterprise: data?.enterprise,
      },
    });
  } catch (error) {
    Toast.show({
      type: 'error',
      text1: 'Error',
      text2: error?.data?.errors[0],
    });
    yield put({
      type: Types.SESSION_SIGNIN_ERROR,
      payload: {error: error?.data?.errors[0]},
    });
  }
}
export function* search({payload}) {
  try {
    const {data} = yield call(api.search, payload);
    yield put({
      type: Types.COMPANY_FILTER_SEARCH_SUCCESS,
      payload: {
        enterprises: data?.enterprises,
      },
    });
  } catch (error) {
    Toast.show({
      type: 'error',
      text1: 'Error',
      text2: error?.data?.errors[0],
    });
    yield put({
      type: Types.SESSION_SIGNIN_ERROR,
      payload: {error: error?.data?.errors[0]},
    });
  }
}
export default all([
  takeLatest(Types.COMPANY_GETALL_REQUEST, getAll),
  takeLatest(Types.COMPANY_FILTER_BYID_REQUEST, getById),
  takeLatest(Types.COMPANY_FILTER_SEARCH_REQUEST, search),
  takeLatest(Types.COMPANY_SAVE_TYPES, getAllTypes),
]);
