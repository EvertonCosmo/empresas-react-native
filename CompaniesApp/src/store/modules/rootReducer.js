import {combineReducers} from 'redux';
import {session, company} from '~store/ducks';
export const rootReducer = combineReducers({
  session,
  company,
});
