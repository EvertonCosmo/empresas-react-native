import {all} from 'redux-saga/effects';
import session from '~store/sagas/session';
import company from '~store/sagas/company';
export default function* rootSaga() {
  return yield all([session, company]);
}
