export const Types = {
  COMPANY_GETALL_REQUEST: 'companies/COMPANY_GETALL_REQUEST',
  COMPANY_GETALL_SUCCESS: 'companies/COMPANY_GETALL_SUCCESS',
  COMPANY_GETALL_ERROR: 'companies/COMPANY_GETALL_ERROR',
  COMPANY_FILTER_BYID_REQUEST: 'company/COMPANY_FILTER_BYID_REQUEST',
  COMPANY_FILTER_BYID_SUCCESS: 'company/COMPANY_FILTER_BYID_SUCCESS',
  COMPANY_FILTER_BYID_ERROR: 'company/COMPANY_FILTER_BYID_ERROR',
  COMPANY_FILTER_SEARCH_REQUEST: 'company/COMPANY_FILTER_SEARCH_REQUEST',
  COMPANY_FILTER_SEARCH_SUCCESS: 'company/COMPANY_FILTER_SEARCH_SUCCESS',
  COMPANY_FILTER_SEARCH_ERROR: 'company/COMPANY_FILTER_SEARCH_ERROR',
  COMPANY_SAVE_TYPES: 'company/COMPANY_SAVE_TYPES',
  COMPANY_SAVE_TYPES_SUCCESS: 'company/COMPANY_SAVE_TYPES_SUCCESS',
};

const INITIAL_STATE = {
  enterprises: null,
  enterprise: null,
  // searchResult: null,
  loading: false,
  types: null,
  error: '',
};
const company = (state = INITIAL_STATE, {type, payload}) => {
  switch (type) {
    case Types.COMPANY_GETALL_SUCCESS:
      return {
        ...state,
        loading: false,
        enterprises: payload?.enterprises,
      };
    case Types.COMPANY_FILTER_BYID_SUCCESS:
      return {
        ...state,
        enterprise: payload?.enterprise,
        loading: false,
        error: '',
      };
    case Types.COMPANY_FILTER_SEARCH_SUCCESS:
      return {...state, loading: false, enterprises: payload?.enterprises};

    case Types.COMPANY_GETALL_ERROR ||
      Types.COMPANY_FILTER_BYID_ERROR ||
      Types.COMPANY_FILTER_SEARCH_ERROR:
      return {
        ...state,
        loading: false,
        enterprises: null,
        searchResult: null,
        error: payload?.error,
      };
    case Types.COMPANY_SAVE_TYPES_SUCCESS:
      return {...state, types: payload?.types};
    default:
      return state;
  }
};
export const Creators = {
  getAll: (isGetTypes = true) => ({
    type: Types.COMPANY_GETALL_REQUEST,
    payload: {isGetTypes},
  }),
  getById: ({id}) => ({
    type: Types.COMPANY_FILTER_BYID_REQUEST,
    payload: {id},
  }),
  search: ({type, name = ''}) => ({
    type: Types.COMPANY_FILTER_SEARCH_REQUEST,
    payload: {type, name},
  }),
};
export default company;
