export const Types = {
  SESSION_LOADING: 'session/SESSION_LOADING',
  SESSION_LOGOUT_REQUEST: 'session/SESSION_LOGOUT',
  SESSION_LOGOUT_SUCCESS: 'session/SESSION_LOGOUT_SUCCESS',
  SESSION_LOGOUT_ERROR: 'session/SESSION_LOGOUT_ERROR',
  SESSION_SIGNIN_REQUEST: 'session/SESSION_SIGNIN',
  SESSION_SIGNIN_SUCCESS: 'session/SESSION_SIGNIN_SUCCESS',
  SESSION_SIGNIN_ERROR: 'session/SESSION_SIGNIN_ERROR',
};
const INITIAL_STATE = {
  investor: null,
  enterprise: null,
  loading: false,
  isAuthenticated: false,
  error: '',
};

const session = (state = INITIAL_STATE, {type, payload}) => {
  switch (type) {
    case Types.SESSION_LOADING:
      return {...state, loading: true};
    case Types.SESSION_SIGNIN_SUCCESS:
      return {
        ...state,
        investor: payload?.investor,
        enterprise: payload.enterprise,
        loading: false,
        isAuthenticated: true,
        error: '',
      };
    case Types.SESSION_LOGOUT_SUCCESS:
      return INITIAL_STATE;
    case Types.SESSION_SIGNIN_ERROR:
      console.log({payload});
      return {...state, loading: false, error: payload?.error};
    case Types.SESSION_LOGOUT_ERROR:
      console.log({payload});
      return {
        ...state,
        loading: false,
        isAuthenticated: false,
        error: payload?.error,
      };

    default:
      return state;
  }
};
export const Creators = {
  signIn: ({email, password}) => ({
    type: Types.SESSION_SIGNIN_REQUEST,
    payload: {email, password},
  }),
  logout: () => ({
    type: Types.SESSION_LOGOUT_REQUEST,
  }),
};
export default session;
