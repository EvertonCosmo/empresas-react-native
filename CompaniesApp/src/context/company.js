import React, {createContext, useContext, useEffect, useState} from 'react';
import {Creators as companyCreators} from '~store/ducks/company';
import {useDispatch} from 'react-redux';
import {useDebouncedCallback} from 'use-debounce/lib';

const CompanyContext = createContext({});

const CompanyProvider = ({children}) => {
  const dispatch = useDispatch();

  const [selectedBadge, setSelectedBadge] = useState(null);
  const [searchTerm, setSearchTerm] = useState(null);

  const debounce = useDebouncedCallback(() => {
    search();
  }, 250);
  function search() {
    if (selectedBadge === 1 && searchTerm === '') {
      dispatch(companyCreators.getAll(false));
    } else {
      dispatch(
        companyCreators.search({
          type: selectedBadge,
          name: searchTerm === null ? '' : searchTerm,
        }),
      );
    }
  }
  useEffect(() => {
    if (searchTerm !== null) {
      debounce();
    }
  }, [searchTerm]);

  return (
    <CompanyContext.Provider value={{setSelectedBadge, setSearchTerm, search}}>
      {children}
    </CompanyContext.Provider>
  );
};
function useCompany() {
  const context = useContext(CompanyContext);
  if (!context) {
    throw new Error('useCompany must be used wrapped by a Provider');
  }

  return context;
}
export {CompanyProvider, useCompany};
