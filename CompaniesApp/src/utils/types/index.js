import {randomColor} from '~utils/colors';

export const enterpriseTypes = {
  All: randomColor(),
  Health: randomColor(),
  Service: randomColor(),
  IT: randomColor(),
  Marketplace: randomColor(),
  Fintech: randomColor(),
  'HR Tech': randomColor(),
  Social: randomColor(),
  Software: randomColor(),
  Transport: randomColor(),
  Music: randomColor(),
  Sports: randomColor(),
  Green: randomColor(),
  Biotechnology: randomColor(),
  IoT: randomColor(),
  Construction: randomColor(),
  Industry: randomColor(),
  Education: randomColor(),
  Games: randomColor(),
};
function indexColor() {
  return enterpriseTypes;
}

(() => indexColor())();
