function getGrettings(currentTime = new Date()) {
  const currentHour = currentTime.getHours();

  const afternoon = 12;
  const evening = 17;
  if (currentHour >= afternoon && currentHour <= evening) {
    return 'Good afternoon,';
  } else if (currentHour >= evening) {
    return 'Good evening,';
  }
  return 'Good Morning,';
}
export {getGrettings};
