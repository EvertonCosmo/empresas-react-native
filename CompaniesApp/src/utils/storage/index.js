import AsyncStorage from '@react-native-async-storage/async-storage';

async function setStorage(data, key) {
  await AsyncStorage.setItem(key, data);
}
async function getStorage(keys = []) {
  const [access_token, client, uid] = await AsyncStorage.multiGet(keys);

  return {
    access_token: access_token[1],
    client: client[1],
    uid: uid[1],
  };
}
export {setStorage, getStorage};
